# Docker for Valve's Steam Deck

This is a simple set of config files for creating a Docker image that
allows you to build `tmux` for the Steam Deck.  Could be a springboard
for building other binaries.

# General

1) Run `make` to create the Docker image.  This will also compile `tmux`.

2) Run `launch-docker.sh` to bring up the Docker container with an
interactive shell.

3) Copy `tmux` to `/host` from inside the container to transfer the binary to the host.

4) On the Steam Deck, make a `~/.local/bin/` directory.  Use `scp` or some
other means to copy `tmux` from the host to the Steam Deck.

5) Update `~/.bash_profile` on the Steam Deck to add `~/.local/bin/` to
the `PATH` var.

6) Due to systemd configuration, `tmux` sessions will not persist across
`ssh` logins.  Create `~/.local/bin/launch-tmux.sh` with this:

		#!/bin/bash
		
		systemd-run --scope --user tmux
	
7) Make `launch-tmux.sh` executable via `chmod a+x launch-tmux.sh`.

8) When launching a new session, use `launch-tmux.sh`, otherwise use
`tmux` directly to attach to previously defined sessions.
