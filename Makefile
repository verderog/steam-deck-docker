DOCKER_HOST_IMAGE_NAME := host-steamdeck

all: .docker-host-steamdeck

.docker-host-steamdeck: Dockerfile
	@echo "Building $(DOCKER_HOST_IMAGE_NAME) docker image"
	docker build -t $(DOCKER_HOST_IMAGE_NAME) --build-arg ARCH=amd64 .
	@touch $@
	
#
# Remove any images that were built, ignoring if they aren't present
#
clean: 
	@echo "Cleaning..."
	@docker image rm $(DOCKER_HOST_IMAGE_NAME) || true
	@rm .docker-host-steamdeck || true
	@echo "Done."
