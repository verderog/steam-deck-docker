ARG ARCH=
FROM ${ARCH}/archlinux:base-devel-20220130.0.46058

# 
# General config setup
#
USER root
SHELL ["/bin/bash", "-c"]


RUN pacman -Sy libevent --noconfirm
RUN pacman -U https://archive.archlinux.org/packages/g/git/git-2.33.1-1-x86_64.pkg.tar.zst --noconfirm

RUN mkdir -p /project
RUN mkdir -p /host
WORKDIR /project
RUN git clone --branch 3.2a https://github.com/tmux/tmux.git
WORKDIR /project/tmux
RUN sh autogen.sh
RUN ./configure && make

