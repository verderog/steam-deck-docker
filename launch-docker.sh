#!/bin/bash


docker_image="host-steamdeck"
if [ $# -eq 1 ]; then
    docker_image=$1
fi


cmd=${@:-"bash"}

interactive_opts=(--interactive --tty)

docker_opts=(
    --rm
    ${interactive_opts[@]}
    --entrypoint /bin/bash
    -v "$(pwd)/host:/host"
    $docker_image
)


docker run ${docker_opts[@]}
